import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue,  BootstrapVueIcons, PopoverPlugin, ToastPlugin, ModalPlugin } from 'bootstrap-vue'
import Home from './views/Home.vue'
import Submit from './views/Submit.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouter from "vue-router"
//import Vuex from 'vuex'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)

Vue.use(PopoverPlugin)
Vue.use(ToastPlugin)
Vue.use(ModalPlugin)

Vue.use(VueRouter)


Vue.use(VueAxios, axios)
//Vue.use(Vuex)
const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: Home },
    { path: '/submit', component: Submit },
  ]
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
