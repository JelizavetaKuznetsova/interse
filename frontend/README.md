# 

## Project setup
```
pnpm install
```

### Compiles and hot-reloads for development
```
pnpm run serve
```

### Compiles and minifies for production
```
pnpm run build
```

### Lints and fixes files
```
pnpm run lint
```
### Dependencies in use:
````
"dependencies": {
    "@popperjs/core": "^2.11.2",
    "bootstrap": "^4.6.1",
    "bootstrap-vue": "^2.21.2",
    "core-js": "^3.6.5",
    "vue": "^2.6.14",
    "vue-router": "^3.5.3"
},
"devDependencies": {
    "@vue/cli-plugin-babel": "~4.5.0",
    "@vue/cli-plugin-eslint": "~4.5.0",
    "@vue/cli-service": "~4.5.0",
    "babel-core": "^6.26.3",
    "babel-eslint": "^10.1.0",
    "babel-loader": "^8.2.3",
    "babel-preset-env": "^1.7.0",
    "eslint": "^6.7.2",
    "eslint-plugin-vue": "^6.2.2",
    "vue-template-compiler": "^2.6.11"
} 
````

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).