# INTERSE

## Description
An application made for Garage48 Student Startup Camp '22.

The aim of the idea was to create a unified Estonian student internship platform. 

As a result we've developed frontend and backend for the platform minimal viable product. 

## Running the application 

For the backend `gradlew bootrun`

For the frontend `npm install` -> `npm run serve` 

## Pictures

![Scheme](images/interse_app_0.jpg)
![Scheme](images/interse_app_1.jpg)
![Scheme](images/interse_app_2.jpg)
![Scheme](images/interse_app_3.jpg)
