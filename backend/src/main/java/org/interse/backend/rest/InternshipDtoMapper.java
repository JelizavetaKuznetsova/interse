package org.interse.backend.rest;
import org.interse.backend.service.Internship;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface InternshipDtoMapper {
    InternshipDto map(Internship internship);

    Internship map(InternshipDto dto);
}
