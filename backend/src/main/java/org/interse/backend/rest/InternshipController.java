package org.interse.backend.rest;

import lombok.AllArgsConstructor;
import org.interse.backend.service.Internship;
import org.interse.backend.service.InternshipService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("api/garage48/internship")
public class InternshipController {

    private final InternshipService internshipService;
    private final InternshipDtoMapper internshipDtoMapper;


    // Get request for all Internships
    @GetMapping
    public ResponseEntity<List<InternshipDto>> getAll() {
        return ResponseEntity.ok(internshipService.findAll().stream().map(internshipDtoMapper::map).collect(Collectors.toList()));
    }

    // Get request for all Internships containing requested company name
    @GetMapping("company/{companyName}")
    public ResponseEntity<List<InternshipDto>> getInternshipByCompany(@PathVariable String companyName) {
        return new ResponseEntity<>(internshipService.getByCompany(companyName), HttpStatus.OK);
    }

    // Get request for all Internships containing requested technologies
    @GetMapping("company/{technology}")
    public ResponseEntity<List<InternshipDto>> getInternshipByTechnology(@PathVariable String technology) {
        return new ResponseEntity<>(internshipService.getByTechnologies(technology), HttpStatus.OK);
    }

    // Get request for sorted by soonest deadline Internships
    @GetMapping("/deadline")
    public ResponseEntity<List<InternshipDto>> getInternshipByDeadline() {
        return new ResponseEntity<>(internshipService.getByDeadline(), HttpStatus.OK);
    }

    // Get request for sorted most recently posted Internships
    @GetMapping("/posttime")
    public ResponseEntity<List<InternshipDto>> getInternshipByPostTime() {
        return new ResponseEntity<>(internshipService.getByPosttime(), HttpStatus.OK);
    }

    // Get request for all Internships containing requested position
    @GetMapping("/position/{position}")
    public ResponseEntity<List<InternshipDto>> getInternshipByPosition(@PathVariable String position) {
        return new ResponseEntity<>(internshipService.getByPosition(position), HttpStatus.OK);
    }

    // Get request for all Internships containing requested position
    @GetMapping("/location/{location}")
    public ResponseEntity<List<InternshipDto>> getInternshipByLocation(@PathVariable String location) {
        List<InternshipDto> internshipDtos = internshipService.getByLocation(location);
        return new ResponseEntity<>(internshipDtos,HttpStatus.OK);
    }

    @GetMapping("/id/{internshipId}")
    public ResponseEntity<InternshipDto> getInternshipById(@PathVariable Long internshipId) {
        return ResponseEntity.ok(internshipDtoMapper.map(internshipService.findInternshipById(internshipId)));
    }

    // Get request for all Internships that come from comserv DB
    @GetMapping("/comserv")
    public ResponseEntity<List<InternshipDto>> getInternshipById() {
        List<InternshipDto> internships = internshipService.getInternshipsFromComserv();
        return ResponseEntity.ok(internships);
    }

    @GetMapping("/futulab")
    public ResponseEntity<List<InternshipDto>> getFutulab() {
        List<InternshipDto> internships = internshipService.getInternshipsFromFutulab();
        return ResponseEntity.ok(internships);
    }

    // Post request for saving new internship
    @PostMapping
    public ResponseEntity<InternshipDto> createInternship(@RequestBody InternshipDto internship) {
        return new ResponseEntity<>(internshipDtoMapper.map(internshipService.save(internshipDtoMapper.map(internship))), HttpStatus.CREATED);
    }
}
