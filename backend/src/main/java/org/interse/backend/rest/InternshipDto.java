package org.interse.backend.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Data
@Accessors(chain = true)
public class InternshipDto {

    private Long id;

    @JsonProperty("company_name")
    private String companyName;

    @JsonProperty("position")
    private String position;

    @JsonProperty("deadline")
    private LocalDate deadline;

    @JsonProperty("post_date")
    private LocalDate postDate;

    @JsonProperty("internship_url")
    private String URL;

    @JsonProperty("logo_url")
    private String logoURL;

    private String description;

    @JsonProperty("location")
    private String location;

    @JsonProperty("technologies")
    private String technologies;

    @JsonProperty("contact-email")
    private String contactEmail;
}
