package org.interse.backend.persistence;
import org.interse.backend.persistence.entity.InternshipDbo;
import org.interse.backend.service.Internship;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface InternshipDboMapper {

    Internship map(InternshipDbo dbo);

    InternshipDbo map(Internship internship);
}
