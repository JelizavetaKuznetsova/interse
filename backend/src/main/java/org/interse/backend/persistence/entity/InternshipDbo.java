package org.interse.backend.persistence.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "internship")
public class InternshipDbo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "company")
    private String companyName;

    @Column(name = "position")
    private String position;

    @Column(name = "deadline")
    private LocalDate deadline;

    @Column(name = "post_date")
    private LocalDate postDate;

    @Column(name = "internship_url")
    private String URL;

    @Column(name = "logo_url")
    private String logoURL;

    @Column(name = "description")
    private String description;

    @Column(name = "location")
    private String location;

    @Column(name = "technologies")
    private String technologies;

    @Column(name = "contact_email")
    private String contactEmail;

    public InternshipDbo() {}
}

