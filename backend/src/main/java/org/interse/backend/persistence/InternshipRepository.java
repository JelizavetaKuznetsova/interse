package org.interse.backend.persistence;

import org.interse.backend.persistence.entity.InternshipDbo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InternshipRepository extends CrudRepository<InternshipDbo, Long> {

    List<InternshipDbo> findAll();

    List<InternshipDbo> findByCompanyNameContaining(String companyName);
}
