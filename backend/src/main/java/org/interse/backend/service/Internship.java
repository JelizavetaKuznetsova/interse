package org.interse.backend.service;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
@Accessors(chain = true)
public class Internship {
    private Long id;

    @NotNull
    private String companyName;

    @NotNull
    private String position;

    //Future
    private LocalDate deadline;

    private LocalDate postDate;

    //@URL
    private String URL;

    private String logoURL;

    @Size(min = 0, max = 400)
    private String description;

    private String location;

    private String technologies;

    private String contactEmail;

}
