package org.interse.backend.service;

import lombok.AllArgsConstructor;
import org.interse.backend.persistence.InternshipDboMapper;
import org.interse.backend.persistence.InternshipRepository;
import org.interse.backend.persistence.entity.InternshipDbo;
import org.interse.backend.rest.InternshipDto;
import org.interse.backend.rest.InternshipDtoMapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class InternshipService {

    private final InternshipRepository internshipRepository;
    private final InternshipDboMapper internshipDboMapper;
    private final InternshipDtoMapper internshipDtoMapper;

    /**
     * Method to get all Internship objects from Repository
     * @return List of all Internships
     */
    public List<Internship> findAll() {
        return internshipRepository.findAll().stream().map(internshipDboMapper::map).collect(Collectors.toList());
    }

    public Internship findInternshipById(Long id) {
        return internshipDboMapper.map(internshipRepository.findById(id).orElse(null));
    }

    /**
     * Method to save internships
     * @param internship Internship to save
     * @return saved Internship(goes through repo which adds id)
     */
    public Internship save(Internship internship) {
        // Validator to check that internship has needed/correct info
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Internship>> violations = validator.validate(internship);
        if (violations.isEmpty())
            return internshipDboMapper.map(internshipRepository.save(internshipDboMapper.map(internship)));
        return null;
    }

    /**
     * Scraping method for comserv's internships
     * @return List of found Internships
     */
    public List<InternshipDto> getInternshipsFromComserv() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        try {

            List<Internship> foundInternships = new ArrayList<>();

            Document document = Jsoup.connect("https://comserv.cs.ut.ee/ati_practice_offers/").get();
            Elements internships = document.getElementsByTag("tr");
            internships = internships.next();

            for (Element internship : internships) {
                Elements internshipInfo = internship.getElementsByTag("td");
                Internship newInternship = new Internship();
                newInternship.setLogoURL(internshipInfo.get(0).getElementsByTag("img").attr("src"));
                newInternship.setCompanyName(internshipInfo.get(1).text());
                newInternship.setPosition(internshipInfo.get(2).text());
                newInternship.setLocation(internshipInfo.get(3).text());
                newInternship.setDeadline(LocalDate.parse(internshipInfo.get(5).text(), formatter));
                newInternship.setContactEmail(internshipInfo.get(6).text());
                save(newInternship);
                foundInternships.add(newInternship);
            }
            List<InternshipDto> internshipDtos = new ArrayList<>();
            foundInternships.forEach(internship -> internshipDtos.add(internshipDtoMapper.map(internship)));
            return internshipDtos;
        } catch (IOException e) {
            return null;
        }
    }

    public List<InternshipDto> getInternshipsFromFutulab() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        try {

            List<Internship> foundInternships = new ArrayList<>();

            Document document = Jsoup.connect("https://futulab.ut.ee/praktika-ja-toopakkumised/?type=internship&sort_wf=Info-%20ja%20kommunikatsioonitehnoloogia&sort_lang=none&sort_country=estonia&sort_county=&sort_city=").get();
            Elements internships = document.getElementsByClass("carousel-item active");
            internships = internships.get(0).getElementsByClass("row");

            for (Element internship : internships) {
                Elements dataSet1 = internship.getElementsByClass("col-lg-4");
                Elements dataSet2 = internship.getElementsByClass("col-lg-3 col-7");

                String companyName = dataSet2.get(0).getElementsByTag("p").get(0).text();
                companyName = companyName.split(":")[1];

                String location = dataSet2.get(0).getElementsByTag("p").get(1).text();
                location = location.split(":")[1];

                String deadline =  dataSet2.get(0).getElementsByTag("p").get(2).text();
                deadline = deadline.split(":")[1].strip();

                Internship newInternship = new Internship();

                newInternship.setCompanyName(companyName);
                newInternship.setLocation(location);
                newInternship.setDeadline(LocalDate.parse(deadline,formatter));

                newInternship.setPosition(dataSet1.get(0).getElementsByTag("a").get(0).text());

                save(newInternship);
                foundInternships.add(newInternship);
            }
            List<InternshipDto> internshipDtos = new ArrayList<>();
            foundInternships.forEach(internship -> internshipDtos.add(internshipDtoMapper.map(internship)));
            return internshipDtos;
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * Method to get all Internships that contain the required position param
     * @param position Parameter for the search
     * @return List of found Internships
     */
    public List<InternshipDto> getByPosition(String position) {
        List<InternshipDto> dtos = new ArrayList<>();
        List<Internship> allInternships = findAll();

        allInternsToListIfEmptyField(dtos, allInternships, position);

        for (Internship internship: allInternships) {
            if (internship.getPosition().toLowerCase(Locale.ROOT).contains(position.toLowerCase(Locale.ROOT)))
                dtos.add(internshipDtoMapper.map(internship));
        }
        return dtos;
    }

    /**
     * Method to get all Internships that contain the required location param
     * @param location Parameter for the search
     * @return List of found Internships
     */
    public List<InternshipDto> getByLocation(String location) {
        List<InternshipDto> dtos = new ArrayList<>();
        List<Internship> allInternships = findAll();

        // If location is left empty, add all Internships
        allInternsToListIfEmptyField(dtos, allInternships, location);

        for (Internship internship : allInternships) {
            if (internship.getLocation().toLowerCase(Locale.ROOT).contains(location.toLowerCase(Locale.ROOT)))
                dtos.add(internshipDtoMapper.map(internship));
        }
        return dtos;
    }

    /**
     * Method to sort all Internships by most recent post-time
     * @return List of sorted Internships
     */
    public List<InternshipDto> getByPosttime() {
        List<InternshipDto> dtos = new ArrayList<>();
        List<Internship> internships = findAll();

        internships.sort(Comparator.comparing(Internship::getPostDate));
        Collections.reverse(internships);
        for (Internship internship : internships) {
            dtos.add(internshipDtoMapper.map(internship));
        }
        return dtos;
    }

    /**
     * Method to sort all Internships by soonest deadline
     * @return List of sorted Internships
     */
    public List<InternshipDto> getByDeadline() {
        List<InternshipDto> dtos = new ArrayList<>();
        List<Internship> internships = findAll();

        internships.sort(Comparator.comparing(Internship::getDeadline));
        for (Internship internship : internships) {
            dtos.add(internshipDtoMapper.map(internship));
        }
        return dtos;
    }

    /**
     * Method to get all Internships with required company name
     * @param companyName Name of the requested company
     * @return List of found Internships
     */
    public List<InternshipDto> getByCompany(String companyName) {
        List<InternshipDto> dtos = new ArrayList<>();
        // If company name is left empty, add all Internships
        if (companyName.replaceAll(" ","").equals("")) {
            List<Internship> internships = findAll();
            for (Internship internship: internships) {
                dtos.add(internshipDtoMapper.map(internship));
            }
        }
        // If company name given, find if any company has the same name
        else {
            List<InternshipDbo> dbos = internshipRepository.findByCompanyNameContaining(companyName);
            for (InternshipDbo elem : dbos) {
                dtos.add(internshipDtoMapper.map(internshipDboMapper.map(elem)));
            }
        }
        return dtos;
    }

    public List<InternshipDto> getByTechnologies(String technology) {
        List<InternshipDto> dtos = new ArrayList<>();
        List<Internship> allInternships = findAll();

        // If technology is left empty, add all Internships
        allInternsToListIfEmptyField(dtos, allInternships, technology);

        for (Internship internship : allInternships) {
            if (internship.getTechnologies().toLowerCase(Locale.ROOT).contains(technology.toLowerCase(Locale.ROOT)))
                dtos.add(internshipDtoMapper.map(internship));
        }
        return dtos;
    }

    private void allInternsToListIfEmptyField(List<InternshipDto> internships, List<Internship> allInternships, String field) {
        // If location is left empty, add all Internships
        if (field.replaceAll(" ","").equals("")) {
            for (Internship internship : allInternships) {
                internships.add(internshipDtoMapper.map(internship));
            }
        }

    }
}
